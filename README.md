# Tugas 3 Jaringan Komputer - Socket Programming

## Anggota Kelompok
Kelompok A5
1. 1806205400 - [Amrisandha Pranantya Prasetyo](https://gitlab.com/amrisandha.prasetyo)
2. 1806205741 - [Muhammad Zakiy Saputra](https://gitlab.com/zakiysaputra)
3. 1806205155 - [Zafira Binta Feliandra](https://gitlab.com/zafirabinta)


## Penjelasan Aplikasi

Aplikasi ini bertujuan untuk menghitung operasi penambahan, pengurangan, perkalian, dan pembagian. Penghitungan ini berdasarkan masukan perintah dari pengguna yang akan dihitung dan dikembalikan oleh server. Aplikasi ini dikembangkan dalam bahasa Python dan menerapkan prinsip master-worker dalam pemrograman socket.

1. [Master Node](https://gitlab.com/amrisandha.prasetyo/tugas-3-socket-programming-jarkom/-/blob/master/master.py) bertujuan sebagai tempat pengguna memasukkan perintah penghitungan. Master node kemudian akan mengirimkan perintah untuk dihitung dan dikirimkan kembali oleh server. Master node ini dapat dijalankan di lokal komputer yang sudah terhubung dengan instance AWS EC2.
2. [Worker Node](https://gitlab.com/amrisandha.prasetyo/tugas-3-socket-programming-jarkom/-/blob/master/worker.py) bertujuan untuk menerima perintah dari master node lalu mengirimkan hasil penghitungan kepada master node kembali. Worker node dapat dijalankan pada server EC2.

---

