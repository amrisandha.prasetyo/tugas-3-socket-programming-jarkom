import socket, threading, time

# HEADER = PANJANG MESSAGE
HEADER = 64
PORT = 6060
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
master.bind(ADDR)

def handle_client(conn, addr):

  print("[CONNECTED] Client connected.")

  connected = True
  
  while connected:

    msg_length = conn.recv(HEADER).decode(FORMAT)
    msg_length = int(msg_length)
    msg = conn.recv(msg_length).decode(FORMAT)

    content = msg.split(" ")

    if content[0] == "ADD":
      x = int(content[1])
      y = int(content[2])
      conn.send("[{0}] {1}".format(ADDR, str(x+y)).encode(FORMAT))

    elif content[0] == "SUBTRACT":
      x = int(content[1])
      y = int(content[2])
      conn.send("[{0}] {1}".format(ADDR, str(x-y)).encode(FORMAT))

    elif content[0] == "MULTIPLY":
      x = int(content[1])
      y = int(content[2])
      conn.send("[{0}] {1}".format(ADDR, str(x*y)).encode(FORMAT))

    elif content[0] == "DIVIDE":
      x = int(content[1])
      y = int(content[2])
      conn.send("[{0}] {1}".format(ADDR, str(x/y)).encode(FORMAT))

    elif content[0] == DISCONNECT_MESSAGE:
      connected = False
    
    else:
      conn.send("I can't do that.")

    #PRINT ADDRESS DAN PESAN
    print("[{0}] {1}".format(ADDR, msg))

  conn.close()


def start():
  master.listen(5)
  print("[LISTENING] Server is listening on {0}".format(SERVER))
  while True:
    conn, addr = master.accept()
    thread = threading.Thread(target=handle_client, args=(conn, addr))
    thread.start()
    
    """
    if thread.is_alive():
      conn.send("[{0}] is alive".format(ADDR))

    else:
      conn.send("[{0}] is not alive".format(ADDR))
    """
    print("[ACTIVE CONNECTIONS] {0}".format(threading.activeCount() - 1))



print("[TEST] if this were printed, then the server are starting soon...")
start()
