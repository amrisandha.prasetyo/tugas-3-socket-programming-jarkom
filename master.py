import socket, random, platform, subprocess

HEADER = 64
PORT = 6060
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
SERVER = None

availablehosts = ['3.95.221.4', '3.87.222.17', '54.91.232.131']
worker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

while True:
    SERVER = random.choice(availablehosts)
    ADDR = (SERVER, PORT)
    state = worker.connect_ex(ADDR)
    if state != 0:
        availablehosts.remove(SERVER)
        print("Server {} is inactive, reconnecting to another server...".format(SERVER))
        if not availablehosts:
            print("All servers inactive. Shutting down...")
            exit()
    else:
        print("Connected to {}".format(SERVER))
        break


def ping(address):
    array = address.split(".")
    addpub = "ec2-" + array[0] + "-" + array[1] + "-" + array[2] + "-" + array[3] + ".compute-1.amazonaws.com"
    # parameter to check whether method is run on windows or not
    param = '-n' if platform.system().lower()=='windows' else '-c'

    # Make command
    command = ['ping', param, '1', addpub]

    if subprocess.call(command) == 0:
        return "Server " + SERVER + " is active and running!"
    else:
        return "Server " + SERVER + " is dead..."

def send(msg):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    worker.send(send_length)
    worker.send(message)
    print(worker.recv(2048).decode(FORMAT))

def disconnect():
    send(DISCONNECT_MESSAGE)

while True:
    print("1: ADD, 2: SUBTRACT, 3: MULTIPLY, 4: DIVIDE")
    print("5: CANCEL CALCULATION, 6: CHECK SERVER STATUS 7: DISCONNECT SERVER")
    x = input('Enter a command number!: ')
    try:
        if x.isdigit():
            if int(x) == 1:
                y = input('Add numbers: ')
                numbers = y.split(", ")
                send("ADD {0} {1}".format(numbers[0], numbers [1]))
            elif int(x) == 2:
                y = input('Subtract numbers: ')
                numbers = y.split(", ")
                send("SUBTRACT {0} {1}".format(numbers[0], numbers [1]))
            elif int(x) == 3:
                y = input('Multiply numbers: ')
                numbers = y.split(", ")
                send("MULTIPLY {0} {1}".format(numbers[0], numbers [1]))
            elif int(x) == 4:
                y = input('Divide numbers: ')
                numbers = y.split(", ")
                send("DIVIDE {0} {1}".format(numbers[0], numbers [1]))
            elif int(x) == 5:
                worker.close()
            elif int(x) == 6:
                print("Trying to check {} Availability by sending ping...".format(SERVER))
                print("========================================================")
                x = ping(SERVER)
                print("========================================================")
                print()
                print(x)
                print()
            elif int(x) == 7:
                disconnect()
                print("DISCONNECTED")
                break
            else:
                print("Invalid number command! Input is not processed by server.")
        else:
            print("Invalid command. Please input the number of your desired command!")
    except ValueError:
        pass
    